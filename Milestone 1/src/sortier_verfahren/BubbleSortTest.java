package sortier_verfahren;

public class BubbleSortTest {
	public static void main (String[]args){
		BubbleSort bs = new BubbleSort();
		
		long[] zahlenliste = {9,4,2,1,3,6,7,8,5};
		//Vor Sortierung
		System.out.println(BubbleSort.array2str(zahlenliste));
		
		bs.sortiere(zahlenliste);
		//Nach Sortierung
		System.out.println(BubbleSort.array2str(zahlenliste));
		System.out.println(bs.getVertauschungen());
	}
}
