package Primzahl;

import java.util.Scanner;

public class Primzahl {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);

		System.out.print("Geben Sie Eine Zahl ein: ");
		long zahl = scan.nextLong();
		System.out.print(prim(zahl));

	}

	public static boolean prim(long zahl) {

		if (zahl <= 2)
			return false;
		for (long i = 2; i < zahl; i++) {

			if (zahl % i == 0) {
				return false;
			}
		}

		return true;

	}

}