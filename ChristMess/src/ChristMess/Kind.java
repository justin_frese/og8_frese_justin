package ChristMess;
import java.util.*;
public class Kind {
	
	//Jedes Kind hat einen Nachnamen, Vornamen, Geburtstag, Wohnort und Bravheitsgrad
	  //Erstellen Sie einen vollparametrisierten Konstruktor, Getter/Setter und eine toString-Methode
	
	private String Nachname;
	  private String Vorname;
	  private String Ort;
	  private int Bravheitsgrad;
	 
	  
	  
	  public String getNachname() {
	    return Nachname;
	  }

	  public void setNachname(String NachnameNeu) {
	    Nachname = NachnameNeu;
	  }

	  public String getVorname() {
	    return Vorname;
	  }

	  public void setVorname(String VornameNeu) {
	    Vorname = VornameNeu;
	  }

	  public String getWohnort() {
	    return Ort;
	  }

	  public void setWohnort(String OrtNeu) {
		  Ort = OrtNeu;
	  }

	  public int getBravheitsgrad() {
	    return Bravheitsgrad;
	  }

	  public void setBravheitsgrad(int BravheitsgradNeu) {
	    Bravheitsgrad = BravheitsgradNeu;
	  }
	
	  public String toString() {
		  String s = "";
		  return s;
	  }
}
